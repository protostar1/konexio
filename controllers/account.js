const validator         = require("validate.js");
const { StatusCodes }   = require("http-status-codes");

const { Account }       = require("../models");

const updateConstraints = {
  status: {
    type: "String",
    presence: true,
  },
}

exports.getAccountInfos      = async (request, response) => {
  var account = []


  try {
    accounts = await Account.findById()
  } catch (e) {
    throw e;
  }

  return response.json(account);
}

exports.updateAccount         = async (request, response) => {
  if (!request.session.loggedIn) return ;
  const updateForm      = request.body;

  const updateFailed    = validator(updateForm, updateConstraints);
  if (updateFailed)       return response.status(StatusCodes.UNAUTHORIZED)
                                       .json(updateFailed);
  try {
    await Account.findByIdAndUpdate({  }, {
      firstname : updateForm.firstname,
      lastname  : updateForm.lastname,
      password  : updateForm.password
    });
  } catch (e) {
    throw e;
  }

  return response.json({ })
}
