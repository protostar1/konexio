const { StatusCodes }   = require("http-status-codes");
const { Account }       = require("../models");

exports.getList = async (request, response) => {
  var accounts = []

  try {
    accounts = await  Account.findMany();
  } catch (e) {
    throw e;
  }
  return response.json(accounts);
}
