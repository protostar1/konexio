const passport          = require("passport");
const validator         = require("validate.js");
const { StatusCodes }   = require("http-status-codes");

const { Account     }   = require("../models")

const loginContraints = {
  mail: {
      type    : "string",
      email   : true,
      presence: true
  },
  password: {
    type    : "string",
    presence: true,
    length: { minimum: 6}
  }
};

const registerConstraints = {
  firstname: {
    type: "string",
     presence: true,
    length: {
      minimum: 1,
      message: "must be at least 1 character"} },
  lastname:  { type: "string", presence: true },
  mail:      { type: "string", email: true, presence: true },
  password:  {
    type: "string",
    presence: true,
    length: {
      minimum: 6,
      message: "must be at least 6 charaters"
    },
  },
  status:              { type: "number",  presence: true }, // No required
  subscribeNewsletter: { type: "boolean", presence: true }  // Default no
};

exports.logout       = async (request, response) => {
  if (request.session.loggedIn) {
    request.session.loggedIn = false;
    request.session._id      = null;
  }
}

exports.loginAccount = async (request, response) => {
  if (request.session.loggedIn) return ;
  const loginForm   = request.body;


  const loginFailed   = validator(loginForm, loginContraints);
  if (loginFailed)  {
    return response.status(StatusCodes.UNAUTHORIZED).json(loginFailed);
  }

  const { mail, password } = loginForm;

  var account

  try {
    account = await Account.findOne({ mail, password });
  }
  catch (e) {
    throw e;
  }
  if (!account) {
    return response.status(StatusCodes.UNAUTHORIZED).send()
  }
  request.session.loggedIn = true;
  request.session._id      = account._id;
  return response.json({ account })
}


const isEmailExists = async (mail) => {
  return await Account.findOne({ mail })
}



exports.registerAccount = async (request, response) => {
  if (request.session.loggedIn) return ;
  const registerForm = request.body;


  registerForm.status = isNaN(registerForm.status) ? 0 : parseInt(registerForm.status);
  registerForm.subscribeNewsletter = (registerForm.subscribeNewsletter) === "true";

  const registerFailed = validator(registerForm, registerConstraints);

  if (registerFailed)   return response.status(StatusCodes.UNAUTHORIZED)
                                       .json(registerFailed);



  var account

  try {

    const alreadyExist = await isEmailExists(registerForm.mail);

    if (alreadyExist) {
      return response.status(StatusCodes.UNAUTHORIZED)
        .json({ reason: "Existe déjà"})
    }

    account = await Account.create({
      firstname           : registerForm.firstname,
      lastname            : registerForm.lastname,
      mail                : registerForm.mail,
      password            : registerForm.password,
      status              : registerForm.status,
      subscribeNewsletter : registerForm.subscribeNewsletter})

  } catch (e) {
    throw e;
  }
  request.session.loggedIn = true;
  request.session.id       = account._id;


  return response.json({ account })
}

exports.accountLogin = passport.authenticate("local", { session: true })
