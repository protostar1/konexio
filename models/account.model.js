const mongoose = require("mongoose");

const accountSchema = new mongoose.Schema({
  firstname    : {
    type    : String,
    required: true
  },
  lastname: {
    type    : String
  },
  mail   : {
    type    : String,
    required: true,
    index   : {
      unique: true
    }},
    password: {
      type    : String,
      required: true
    },
    status: {
      type: Number
    },
    subscribeNewsletter: {
      type: Boolean
    }
});

module.exports = accountSchema;
