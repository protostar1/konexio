const mongoose          = require("mongoose");

const accountSchema     = require("./account.model")
const { mongodb }       = require("../settings.json")

mongoose.connect(mongodb.uri, {
  useNewUrlParser   : true,
  useUnifiedTopology: true
});

const Account   = mongoose.model("Account", accountSchema);

exports.Account = Account;
