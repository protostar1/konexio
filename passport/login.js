const passport      = require("passport");
const LocalStrategy = require("passeport-local").Strategy;

const strategy = new LocalStrategy(
  (mail, password, done) => {
    Account.findOne({ mail }, (error, account) => {
      if (error)    { return done(error) };
      if (!account) { return done(null, false, { message: "Incorrect username. "})}
      if (!account.validPassword(password)) {
        return done(null, false, { message: "Incorrect password"})
      }
      done(null, account);
    })
  }
)

module.exports = strategy;
