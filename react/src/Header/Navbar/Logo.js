import React                from "react";
import ReactDOM             from "react-dom";

export default class Logo extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <a className="brand-logo" href="#">
          {this.props.text}
        </a>
      </div>
    )
  }
}
