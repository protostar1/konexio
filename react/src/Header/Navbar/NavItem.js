import React                from "react";
import ReactDOM             from "react-dom";
import {
  Link
}                           from "react-router-dom";

const NavItem = (props) =>
  (<li>
      <Link to={props.to}>{props.children}</Link>
   </li>)

export default NavItem;
