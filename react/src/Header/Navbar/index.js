import React                from "react";
import ReactDOM             from "react-dom";
import {
  Link
}                           from "react-router-dom";


import "materialize-css/dist/js/materialize.js";
import "materialize-css/dist/css/materialize.css";
import { Navbar }  from "react-materialize";

import Logo                 from "./Logo"

const   AS_USER  = 1;
const   AS_GUEST = 2;

const NAVS = [
  {path: "/",         text: "Liste des utilisateurs"},
  {path: "login",     text: "Se connecter", AS_GUEST},
  {path: "register",  text: "S'inscrire"  , AS_GUEST},
  {path: "users",     text: "Éditer mon profil", AS_USER},
  {path: "logout",    text: "Se déconnecter", AS_USER},

]

export default class extends React.Component {

  render() {
    const isLoggedIn = document.cookie.indexOf("konexio_session") != -1;
    return (
      <Navbar
        alignLinks="right"
        brand={<Logo text="Konexio"/>}
        centerChildren
        id="mobile-nav"
        options={{
          edge            : "left",
          draggable       : true,
          inDuration      : 250,
          onCloseEnd      : null,
          onCloseStart    : null,
          onOpenEnd       : null,
          onOpenStart     : null,
          outDuration     : 200,
          preventScrolling: true
        }}
        >

        {NAVS.map((nav, key) => {
          if (nav.AS_USER == AS_USER && !isLoggedIn) return ;
          if (nav.AS_GUEST == AS_GUEST && isLoggedIn) return ;
          return (<Link key={key} to={nav.path}>{nav.text}</Link>);
      })}
      </Navbar>
    )
  }
}
