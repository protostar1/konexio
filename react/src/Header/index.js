import React                from "react";
import ReactDOM             from "react-dom";
import {
  Link
}                           from "react-router-dom";

import "materialize-css/dist/js/materialize.js";
import "materialize-css/dist/css/materialize.css";

import Navbar               from "./Navbar";

export default class Header extends React.Component {

  render() {
    return (
      <div>
        <Navbar />
      </div>
    )
  }
}
