import React    from "react";
import ReactDOM from "react-dom";

import { DataGrid } from '@material-ui/data-grid';
import Avatar       from "../Register/defaultAvatar.png"

import Header   from "../Header";

const columns = [
  { field: "id",       headerName: "ID",          width: 70 },
  { field: "avatar",   headerName: "Avatar",      width: 100, renderCell(src) { return <img src={src} alt="avatar" /> } },
  { field: "fullName", headerName: "Nom complet", width: 300},
];

const rows = [
  {id: 0, avatar: <Avatar />, fullName: "xd"},
  {id: 1, avatar: "lol", fullName: "xs s"}
]

export default class Home extends React.Component {

  constructor(props) {
    super(props);

    this.setState = {
      rows: []
    }
    this.updateRows = this.updateRows.bind(this)
    this.updateRows();
  }

  updateRows() {
    return fetch("/api/v1/accounts/list", { method: "GET", credentials: "same-origin" })
      .then(response => response.json())
      .then(rows     => this.setState({ rows }))
  }

  render() {
    return (
      <div className="container">
        <div className="center-align z-depth-6 card-panel">
          {this.rows ? (
          <div style={{ height: 300, width: '100%' }}>
            <DataGrid rows={this.state.rows} columns={columns}    checkboxSelection={false}  />
          </div>) : <p>Aucun utilisateur trouvé</p>}
        </div>
      </div>
    )
  }
}
