import React    from "react";
import ReactDOM from "react-dom";
import {
  Link
}               from "react-router-dom";

import MailIcon from '@material-ui/icons/Mail';
import LockIcon from '@material-ui/icons/Lock';

import Checkbox from "@material-ui/core/Checkbox";

import Header   from "../Header";
import Alert    from "../Utils/Alert";

const     HTTP_UNAUTHORIZED = 401;
const     HTTP_OK           = 200;

export default class Login extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      mail      : "",
      password  : "",
      rememberMe: true,
      error     : null
    }

    this.onChangePassword   = this.onChangePassword.bind(this);
    this.onChangeMail       = this.onChangeMail.bind(this);
    this.onSubmit           = this.onSubmit.bind(this);
  }

  onSubmit() {
    const headers   = new Headers();
    headers.append("Content-Type", "application/json");

    const body      = JSON.stringify({
      mail      : this.state.mail,
      password  : this.state.password
    });

    return fetch("/api/v1/login", { method: "POST", body, headers, credentials: "same-origin" })
      .then(response => response.json().then(json => {
              if (response.status == HTTP_OK) {
                this.props.history.push("/");
                return ;
              }
              this.setState({ error: json })
            }
          ))
  }

  onChangeMail(event) {
    const mail = event.target.value;
    this.setState({ mail });
  }

  onChangePassword(event) {
    const password = event.target.value;
    this.setState({ password });
  }

  render() {
    var error = this.state.error;
    if (error)
      error =  error[Object.keys(error)[0]]

    const getFieldStatus = (name) => {
      if (!this.state.error) return ;
      return this.state.error[name] ? "invalid" : "valid"
    }

    return (
      <div className="container">
        <div className="center-align col s12 z-depth-6 card-panel">
          {error ? <Alert text={error} /> : ""}
          <form className="login-form">
            <div className="row"></div>
            <div className="row">
              <div className="input-field col s12">
                <MailIcon className="prefix" />
                <input id="email"
                  className={getFieldStatus("mail")}
                  value={this.state.mail}
                  onChange={this.onChangeMail}
                  type="email" />
                <label htmlFor="email">Adresse e-mail</label>
              </div>
            </div>

          <div className="row">
            <div className="input-field col s12">
              <LockIcon className="prefix" />
              <input id="password"
                className={getFieldStatus("password")}
                value={this.state.password}
                onChange={this.onChangePassword}
                type="password" />
              <label htmlFor="password">Mot de passe</label>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s12">
              <a href="#" onClick={this.onSubmit} className="btn waves-effect waves-light col s12">Se connecter</a>
            </div>
          </div>

          <div className="row">
            <div className="input-field col s6 m6 l6">
              <p className="margin medium-small">
                <Link to="/register">S'inscrire</Link>
              </p>
            </div>
          </div>

        </form>
      </div>
      </div>)
  }
}
