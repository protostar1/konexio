import React    from "react";
import ReactDOM from "react-dom";

import MenuItem from '@material-ui/core/MenuItem';
import Select   from '@material-ui/core/Select';

import MailIcon from '@material-ui/icons/Mail';
import LockIcon from '@material-ui/icons/Lock';

import Checkbox from "@material-ui/core/Checkbox";

import Avatar   from "./defaultAvatar.png";
import Alert    from "../Utils/Alert";

const HTTP_OK = 200;

const STATUS = [
  "Étudiant",
  "Enseignant",
  "Enseignant assitant"
]


export default class Profile extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      firstname : "",
      lastname  : "ssss",
      mail      : "",
      status    : 0,
      error     : null
    }

    this.avatar            = document.createElement("input");

    this.avatar.setAttribute("type", "file");
    this.avatar.setAttribute("accept", "image/png, image/jpeg")

    this.onChangeMail       = this.onChangeMail.bind(this);
    this.onChangeFirstname  = this.onChangeFirstname.bind(this)
    this.onChangeLastname   = this.onChangeLastname.bind(this)
    this.onChangeStatus     = this.onChangeStatus.bind(this)
    this.uploadAvatar       = this.uploadAvatar.bind(this)
    this.onSubmit           = this.onSubmit.bind(this);
    this.getAccountInfos    = this.getAccountInfos.bind(this);

    this.getAccountInfos();
  }

  getAccountInfos() {
    return fetch("http://localhost:3000/account/infos", { method: "GET" })
      .then(response => response.json())
      .then(account     => this.setState({
        firstname: account.firstname,
        lastname:  account.lastname,
        mail:      account.mail,
        status:    account.status}))
  }

  uploadAvatar() {
    this.avatar.click();
  }

  onSubscribeNewsletter() {
    this.setState({ subscribeNewsletter: !this.state.subscribeNewsletter })
  }

  onAcceptCGU() {
    this.setState({ acceptCGU: !this.state.acceptCGU })
  }

  onChangePassword(event) {
    this.setState({ password: event.target.value  })
  }

  onChangePasswordConfirmation(event) {
    this.setState({ passwordConfirmation: event.target.value })
  }

  onChangeFirstname(event) {
    this.setState({ firstname: event.target.value  })
  }

  onChangeLastname(event) {
    this.setState({ lastname: event.target.value })
  }

  onChangeMail(event) {
    this.setState({ mail: event.target.value })
  }

  onChangeStatus(event) {
    const status = event.target.value;
    this.setState({ status })
  }

  displayError(error) {
    this.setState({ error });
  }

  onSubmit() {
    var {
      firstname,
      lastname,
      mail,
      status } = this.state;


    const body = new FormData();

    body.append("firstname",           firstname);
    body.append("lastname" ,           lastname);
    body.append("mail",                mail);
    body.append("status",              status);
    body.append("avatar"             , this.avatar.files[0]);

    return fetch("http://localhost:3000/api/v1/account/update", { method: "POST", body, credentials: "same-origin"})
      .then(response => response.json().then(json => {
              if (response.status == HTTP_OK) {
                this.setState({ error: null })
                console.log("success", json);
                return ;
              }
              console.log(json)
              this.setState({ error: json })
            }
          ))
  }

  onChangeMail(event) {
    const mail = event.target.value;
    this.setState({ mail });
  }

  render() {
    var error = this.state.error;
    if (error)
      error =  error[Object.keys(error)[0]]

  const getFieldStatus = (name) => {
    if (!this.state.error) return ;
    return this.state.error[name] ? "invalid" : ""
  }
    return (
      <div>
      <div className="container">
        <div className="center-align z-depth-6 card-panel">
          {error ? <Alert text={error} /> : ""}
          <form className="register-form col s14">
            <div className="row">
              <div className="col s2">
                <img src={Avatar} alt="avatar" className="circle responsive-img" onClick={this.uploadAvatar} />
              </div>
            </div>

            <div className="row">
              <div className="input-field">
                <Select className="col s12"
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={this.state.status}
                      onChange={this.onChangeStatus}>
                      {STATUS.map((status, key) =>
                        <MenuItem key={key} value={key}>{status}</MenuItem>)}
                    </Select>
              </div>
            </div>

            <div className="row">
              <div className="input-field col s6">
                <MailIcon className="prefix" />
                <input id="firstname" type="text" value={this.state.firstname} onChange={this.onChangeFirstname} className={getFieldStatus("firstname")} />

              </div>
              <div className="input-field col s6">
                <MailIcon className="prefix" />
                <input disabled id="lastname"  type="text"  value={this.state.lastname} onChange={this.onChangeLastname} className={getFieldStatus("lastname")} />

              </div>
            </div>

            <div className="row">
              <div className="input-field col s12">
                <MailIcon className="prefix" />
                <input disabled id="mail" type="text" value={this.state.mail} onChange={this.onChangeMail} className={getFieldStatus("mail")} />

              </div>
            </div>


            <div className="row">
              <div className="input-field m6 center-align">
                <a href="#" onClick={this.onSubmit} className="btn waves-effect waves-light col s6">mettre à jour les informations de mon compte</a>
              </div>
            </div>

          </form>
        </div>
      </div>
      </div>
    )
  }
}
