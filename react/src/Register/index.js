import React    from "react";
import ReactDOM from "react-dom";

import MenuItem from '@material-ui/core/MenuItem';
import Select   from '@material-ui/core/Select';

import MailIcon from '@material-ui/icons/Mail';
import LockIcon from '@material-ui/icons/Lock';

import Checkbox from "@material-ui/core/Checkbox";

import Avatar   from "./defaultAvatar.png";
import Alert    from "../Utils/Alert";

const HTTP_OK = 200;

const STATUS = [
  "Étudiant",
  "Enseignant",
  "Enseignant assitant"
]


export default class Register extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      firstname : "",
      lastname  : "",
      mail      : "",
      password  : "",
      passwordConfirmation : "",
      status    : 2,
      subscribeNewsletter : false,
      acceptCGU           : false
    }


    this.avatar            = document.createElement("input");

    this.avatar.setAttribute("type", "file");
    this.avatar.setAttribute("accept", "image/png, image/jpeg")

    this.onChangePassword             = this.onChangePassword.bind(this);
    this.onChangePasswordConfirmation = this.onChangePasswordConfirmation.bind(this)
    this.onChangeMail       = this.onChangeMail.bind(this);
    this.onChangeFirstname  = this.onChangeFirstname.bind(this)
    this.onChangeLastname   = this.onChangeLastname.bind(this)
    this.onChangeStatus     = this.onChangeStatus.bind(this)
    this.uploadAvatar       = this.uploadAvatar.bind(this)
    this.onAcceptCGU        = this.onAcceptCGU.bind(this)
    this.onSubscribeNewsletter  = this.onSubscribeNewsletter.bind(this)
    this.onSubmit           = this.onSubmit.bind(this);
  }

  uploadAvatar() {
    this.avatar.click();
  }

  onSubscribeNewsletter() {
    this.setState({ subscribeNewsletter: !this.state.subscribeNewsletter })
  }

  onAcceptCGU() {
    this.setState({ acceptCGU: !this.state.acceptCGU })
  }

  onChangePassword(event) {
    this.setState({ password: event.target.value  })
  }

  onChangePasswordConfirmation(event) {
    this.setState({ passwordConfirmation: event.target.value })
  }

  onChangeFirstname(event) {
    this.setState({ firstname: event.target.value  })
  }

  onChangeLastname(event) {
    this.setState({ lastname: event.target.value })
  }

  onChangeMail(event) {
    this.setState({ mail: event.target.value })
  }

  onChangeStatus(event) {
    const status = event.target.value;
    this.setState({ status })
  }

  displayError(error) {
    this.setState({ error });
  }

  onSubmit() {
    var {
      firstname,
      lastname,
      mail,
      status,
      password,
      passwordConfirmation,
      acceptCGU,
      subscribeNewsletter } = this.state;

      if (password != passwordConfirmation) {
        return this.displayError({
          password: "Les deux mots de passe ne sont pas identiques",
          passwordConfirmation: "" })
      }
      if (!acceptCGU) {
        return this.displayError({
           acceptCGU: "Veuillez accepter les conditions générales d'utilisation" })
      }

    const body = new FormData();

    body.append("firstname",           firstname);
    body.append("lastname" ,           lastname);
    body.append("mail",                mail);
    body.append("status",              status);
    body.append("password",            password);
    body.append("subscribeNewsletter", subscribeNewsletter);
    body.append("avatar"             , this.avatar.files[0]);

    return fetch("/api/v1/register", { method: "POST", body, credentials: "include" })
      .then(response => response.json().then(json => {
              if (response.status == HTTP_OK) {
                this.setState({ error: null })
                this.props.history.push("/");
                return ;
              }
              console.log(json)
              this.setState({ error: json })
            }
          ))
  }

  onChangeMail(event) {
    const mail = event.target.value;
    this.setState({ mail });
  }

  onChangePassword(event) {
    const password = event.target.value;
    this.setState({ password });
  }

  render() {
    var error = this.state.error;
    if (error)
      error =  error[Object.keys(error)[0]]

  const getFieldStatus = (name) => {
    if (!this.state.error) return ;
    return this.state.error[name] ? "invalid" : ""
  }
    return (
      <div>
      <div className="container">
        <div className="center-align z-depth-6 card-panel">
          {error ? <Alert text={error} /> : ""}
          <form className="register-form col s14">
            <div className="row">
              <div className="col s2">
                <img src={Avatar} alt="avatar" className="circle responsive-img" onClick={this.uploadAvatar} />
              </div>
            </div>

            <div className="row">
              <div className="input-field">
                <Select className="col s12"
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      value={this.state.status}
                      onChange={this.onChangeStatus}>
                      {STATUS.map((status, key) =>
                        <MenuItem key={key} value={key}>{status}</MenuItem>)}
                    </Select>
              </div>
            </div>

            <div className="row">
              <div className="input-field col s6">
                <MailIcon className="prefix" />
                <input id="firstname" type="text" value={this.state.firstname} onChange={this.onChangeFirstname} className={getFieldStatus("firstname")} />
                <label htmlFor="firstname">Prénom</label>
              </div>
              <div className="input-field col s6">
                <MailIcon className="prefix" />
                <input id="lastname"  type="text"  value={this.state.lastname} onChange={this.onChangeLastname} className={getFieldStatus("lastname")} />
                <label htmlFor="lastname">Nom de famille</label>
              </div>
            </div>

            <div className="row">
              <div className="input-field col s12">
                <MailIcon className="prefix" />
                <input id="mail" type="text" value={this.state.mail} onChange={this.onChangeMail} className={getFieldStatus("mail")} />
                <label htmlFor="mail">Adresse e-mail</label>
              </div>
            </div>

            <div className="row">
              <div className="input-field col s6">
                <LockIcon className="prefix" />
                <input id="password" type="password" value={this.state.password} onChange={this.onChangePassword} className={getFieldStatus("password")} />
                <label htmlFor="password">Mot de passe</label>
              </div>
              <div className="input-field col s6">
                <LockIcon className="prefix" />
                <input id="passwordConfirmation" type="password" value={this.state.passwordConfirmation} onChange={this.onChangePasswordConfirmation} className={getFieldStatus("passwordConfirmation")} />
                <label htmlFor="passwordConfirmation">Confirmation de mot de passe</label>
              </div>
            </div>

            <div className="row">
              <div className="input-field col s6">
                <label>
                <input type="checkbox" className="filled-in" checked={this.state.subscribeNewsletter ? "checked" : ""} onChange={this.onSubscribeNewsletter} />
                <span>Je m'abonne à la newsletter</span>
                </label>
              </div>
              <div className="input-field col s6">

              <label>
                <input type="checkbox" className="filled-in" checked={this.state.acceptCGU ? "checked" : ""} onChange={this.onAcceptCGU} />
                <span>J'accepte les termes et les conditions générales d'utilisation</span>
              </label>
              </div>
            </div>

            <div className="row">
              <div className="input-field m6 center-align">
                <a href="#" onClick={this.onSubmit} className="btn waves-effect waves-light col s6">S'inscrire</a>
              </div>
            </div>

          </form>
        </div>
      </div>
      </div>
    )
  }
}
