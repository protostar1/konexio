import React    from "react";
import ReactDOM from "react-dom";

import  "./Alert.css";

const Alert = props => (
  <div className="alert error">
    {props.text}
  </div>)

export default Alert;
