import React    from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Header   from "./Header";

import Home     from "./Home";
import Login    from "./Login";
import Register from "./Register";
import Profile  from "./Profile";

class Konexio extends React.Component {
  render() {
    return (
      <Router>
        <div>
        <Header />
        <Switch>
          <Route exact path="/"         component={Home}      />
          <Route exact path="/login"    component={Login}    />
          <Route exact path="/register" component={Register} />
          <Route exact path="/users"  component={Profile}  />
        </Switch>
        </div>
      </Router>
    )
  }
}

ReactDOM.render(<Konexio />, document.getElementById("root"));
