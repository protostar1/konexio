const express        = require("express")


const authController    = require("../../controllers/auth");
const accountController = require("../../controllers/account")
const accountsController = require("../../controllers/accounts")

const router  = express.Router();

router.post("/login",         authController.loginAccount);
router.post("/register",      authController.registerAccount);

router.get("/accounts/list",    accountsController.getList);

router.post("/account/update",  accountController.updateAccount);
router.post("/account/infos" ,  accountController.getAccountInfos);

module.exports = router;
