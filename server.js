const express          = require("express");
const cors             = require("cors");
const bodyParser       = require("body-parser");
const formData         = require("express-form-data");
const path             = require("path");
const session          = require('express-session')

const settings         = require("./settings.json");

const routes           = require("./routes");

const PORT    = process.env.PORT              || settings.server.port;
const SECRET  = process.env.SERVER_SECRET_KEY || settings.server.secretKey;

const app     = express();

app.use(cors());


app.use(session({
  secret            : SECRET,
  name              :"konexio_session",
  saveUninitialized :false}))
app.use(formData.parse())
app.use(bodyParser.json());
app.use('/', routes);

app.use(express.static(path.join(__dirname, "react/dist")))

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, "react/dist/index.html"))
})

app.listen(PORT, () => {
  console.log("Listen on the port", PORT)
});
